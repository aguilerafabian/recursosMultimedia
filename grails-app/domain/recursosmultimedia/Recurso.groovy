package recursosmultimedia

class Recurso {

    public static String TYPE_LAMP      = "lamp"
    public static String TYPE_PROJECTOR = "projector"
    public static String TYPE_POINTER   = "laserpointer"
    public static String TYPE_COMPUTER  = "computer"

    String  nombre
    String  tipo
    Date    expirationDate // expiration_date - para acceso via API

    Date    creationDate
    Date    lastModifiedDate

    static constraints = {
        tipo                (nullable: true)
        nombre              (nullable: true)
        creationDate        (nullable: true)
        lastModifiedDate    (nullable: true)
    }

    def beforeInsert() {
        creationDate = new Date()
        lastModifiedDate = creationDate
    }

    def beforeUpdate() {
        lastModifiedDate = new Date()
    }
}
