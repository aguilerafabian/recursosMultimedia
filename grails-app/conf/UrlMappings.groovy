class UrlMappings {

	static mappings = {

		"/recursosmultimedia/search" (controller: "search") {
			action = [ GET : "get" ]
		}

		"/recursosmultimedia/$id?" (controller: "recurso") {
			action = [ GET : "get", POST : 'post', PUT : 'put', DELETE: 'delete' ]
		}

        "/"(view:"/index")
        "500"(view:'/error')
	}
}
