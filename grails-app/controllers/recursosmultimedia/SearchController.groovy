package recursosmultimedia

import grails.converters.*

class SearchController {

    def recursoService

    def get() {

        log.info ("Llego un request al GET action search...")

        def parametros = params

        log.info ("parametros de entrada al search: ${parametros}")
        def recursos = recursoService.searchRecurso(parametros)

        response.status = 200

        if (!recursos) {
            log.info ("No hay registros con ese patron de busqueda")
            render recursos as JSON
            return
        }

        def listaArreglada = []
        recursos.each() { it ->
            listaArreglada.add (recursoService.getResponseMap(it))
        }
        render listaArreglada as JSON
	}
}
