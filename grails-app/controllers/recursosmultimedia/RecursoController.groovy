package recursosmultimedia

import grails.converters.*
import recursosmultimedia.ErrorConstants

class RecursoController {

    def recursoService
    def validationService

    def index() {

    }

    def post() {

        log.info ("Llego un request al POST action creo uno nuevo...")

        def parametros = request.JSON

        log.info ("parametros de entrada: ${parametros}")

        def recurso

        try {
            validationService.validate("recurso", "post", parametros)
            log.info ("Paso las validaciones...")
            recurso = recursoService.crearRecurso(parametros)
        } catch (Exception e) {
            log.info ("hay error en la invocacion")
            log.info (e.getMessage())

            response.status = 400

            def map = [
                error: [
                    code    : ErrorConstants.MISSING_PARAMETER,
                    message : e.getMessage()
                ]
            ]
            render map as JSON
            return
        }


        if (recurso) {
            response.status = 201

            Map responseMap = recursoService.getResponseMap(recurso)
            render responseMap as JSON
            return
        } else {
            log.info ("error....")
            response.status = 500
        }

        render recurso as JSON
    }

    def get() {

        log.info ("Llego un request al GET action al controlador...")

        def parametros = params

        log.info ("parametros de entrada: ${parametros}")
        def recurso = recursoService.obtenerRecurso(parametros.id)

        if (recurso) {
            response.status = 200
            Map responseMap = recursoService.getResponseMap(recurso)
            render responseMap as JSON
            return
        } else {
            log.info ("no estaba en este GET...")
            response.status = 404
        }

        render recurso as JSON
    }


    def delete() {

        log.info ("Llego un request al DELETE action al controlador...")

        def parametros = params

        log.info ("parametros de entrada: ${parametros}")
        def recurso = recursoService.eliminarRecurso(parametros.id)

        if (recurso) {
            log.info ("se elimino...")
            response.status = 200
        }else {
            log.info ("no se pudo eliminar...")
            response.status = 404
        }

        render recurso as JSON
    }


    def put() {

      log.debug("Llego un request al PUT action")
      def parametros = request.JSON
      def id = params.id

      log.debug("parametros de entrada: Id: ${id}, ${parametros}")
      def recurso = recursoService.actualizarRecurso(id, parametros)

      log.debug("vuelta al controlador")
      if (recurso){
        log.debug("Se actualizo el recurso: ${recurso.nombre}, ${recurso.tipo}")

        Map responseMap = recursoService.getResponseMap(recurso)
        render responseMap as JSON
        return
      } else{
        log.debug("No existia el recurso: ID: ${id}")
      }

      render recurso as JSON

    }
}
