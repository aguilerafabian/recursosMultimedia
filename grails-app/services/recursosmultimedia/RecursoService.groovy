package recursosmultimedia

import grails.transaction.Transactional

import recursosmultimedia.Computer
import recursosmultimedia.Lamp
import recursosmultimedia.Proyector
import recursosmultimedia.Recurso

@Transactional
class RecursoService {

    def crearRecurso(def params) {

        def recurso

        log.info ("crearRecurso() starts...")

        if ( !(params.tipo in [Recurso.TYPE_PROJECTOR, Recurso.TYPE_COMPUTER, Recurso.TYPE_LAMP])) {
            log.info ("parametro tipo: ${params.tipo} no valido")
            throw new Exception ("Not valid 'tipo' parameter")
        }

        switch (params.tipo) {

            case Recurso.TYPE_LAMP :
                log.info ("Crea un nuevo '${Recurso.TYPE_LAMP}'")
                recurso = new Lamp()
                recurso.tipo = Recurso.TYPE_LAMP
                break;

            case Recurso.TYPE_PROJECTOR :
                log.info ("Crea un nuevo '${Recurso.TYPE_PROJECTOR}'")
                recurso = new Proyector()
                recurso.tipo = Recurso.TYPE_PROJECTOR
                break;

            case Recurso.TYPE_COMPUTER :
                log.info ("Crea un nuevo '${Recurso.TYPE_COMPUTER}'")
                recurso = new Computer()
                recurso.tipo = Recurso.TYPE_COMPUTER
                break;
        }
        recurso.nombre = params.nombre

        // como se maneja la fecha ? (usar el iso 8601)
        //recurso.expirationDate = params.expirationDate

        recurso.expirationDate = Calendar.getInstance().getTime()

        if (!recurso.save(flush:true)) {
            log.error(recurso.errors)
        }

        log.info ("nuevo recurso, ID: ${recurso.id}")

        recurso
    }

    def obtenerRecurso(String id) {

        Recurso recurso = Recurso.findById(id.toLong())

        if (!recurso) {
            log.info ("no esta el recurso, ID: ${id}")
        }

        recurso
    }

    def searchRecurso(def parametros) {

        def recursos
        if (parametros.tipo && !parametros.nombre) {

            log.info("solo busca por TIPO")
            recursos = Recurso.findAllByTipo(parametros.tipo)
        }

        if (!parametros.tipo && parametros.nombre) {

            log.info("solo busca por NOMBRE")
            recursos = Recurso.findAllByNombre(parametros.nombre)
        }

        if (parametros.tipo && parametros.nombre) {

            log.info("busca por NOMBRE y TIPO")
            recursos = Recurso.findAllByNombreAndTipo(parametros.nombre, parametros.tipo)
        }

        recursos
    }


    def eliminarRecurso(String id) {

            Recurso recurso = Recurso.findById(id.toLong())

            if (recurso) {
                recurso.delete()
                log.debug ("se elimino el recurso ID: ${id}")

            } else {
                log.debug ("no esta el recurso, ID: ${id}")
            }

            recurso
    }

    def actualizarRecurso(String id, def parametros) {

      log.debug("Entro al servicio actualizarRecurso")
      def recurso = Recurso.findById(id)
      log.debug("Busca el recurso")

      if (recurso){
        recurso.nombre = parametros.nombre
        recurso.tipo = parametros.tipo
        recurso.save(flush:true)
        log.debug("Actualizo el Recurso")

      } else{
        log.debug("No se encontro el recuso")
      }

      recurso
    }

    Map getResponseMap (Recurso recurso) {

        Map responseMap = [
            id                  : recurso.id,
            nombre              : recurso.nombre,
            tipo                : recurso.tipo,
            creation_date       : recurso.creationDate,
            lastModified_date   : recurso.lastModifiedDate,
        ]
    }
}
