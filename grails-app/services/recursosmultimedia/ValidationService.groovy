package recursosmultimedia

import grails.transaction.Transactional

@Transactional
class ValidationService {

    def validate(String from, String action, def params) {

      log.info("entra a la validacion")

        switch (from) {
                case "recurso" : validateRecurso(action, params)
                break
        }
    }

    def validateRecurso(String action, def params) {

        if (action == "post") {

            if (!params.nombre) {

                String message = "Missing 'nombre' param"

                log.info(message)
                throw new Exception (message)
            }

            if (!params.tipo) {
                String message = "Missing 'tipo' param"

                log.info(message)
                throw new Exception (message)
            }

            if (params.expirationDate) {

                // validamos que venga con un formato correcto
                // si no ... sale con Exception
            }
        }
    }
}
